# how-git-works

Example files for a talk about how Git works.

## Explanation

For each example, I took a copy of a simple Git repo before and after running
a simple command.

E.g. inside add/before is a copy of the repo after I created a file, but before
I ran `git add`.  Inside add/after is a copy of the same repo after I ran
`git add hello.py`.

## License

Copyright 2019 Andy Balaam and CB Bailey.

Released under the [MIT License](LICENSE).
